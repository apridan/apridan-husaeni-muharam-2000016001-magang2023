## Detail Kegiatan Magang
#### Nama                    : Apridan Husaeni Muharam
#### NIM                      : 2000016001
#### Organisasi Tempat Magang : Biro Sistem Informasi UAD (BSI)
#### Judul Kerja Praktek      : Sistem Admin 
#### Dosen Pembimbing         : Ibu Azty Acbarrifha Nour, S.T., M.Eng
#### Pembimbing Lapangan      : Bapak Wahyu
---
### Link Milestone : [Klik Saya](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/milestones?sort=due_date_desc&state=all)

### Link Log Book : [Klik Saya](https://docs.google.com/document/d/1Bbd6Bt5S7pEgmTeWDhuH-fFVT1lu9cm3/edit?usp=sharing&ouid=115431646105977913090&rtpof=true&sd=true)
---
### Berikut adalah detail kegiatan magang ;

#### 1. Membuat aplikasi login menggunakan framework CI 3 yang telah di custom oleh BSI (14-24 Maret 2023)

|No | Task | Tanggal | Keterangan | 
| - | :--: | :----------: | -------- |
| 1 |Memahami framework yang diberikan|14 - 17 Maret 2023 | - |
| 2 |Mengkoneksikan Framework dengan database di server BSI |18 Maret 2023 |Pada project latihan ini database telah dibuat dan database disimpan di server BSI dan diminta untuk dikoneksikan |
| 3 |Membuat fungsi login |18 - 20 Maret 2023 |Tampilan login menggunakan css, dan bootstrap. |
| 4 |Membuat fungsi super admin untuk pengelolaan user|20 - 24 Maret 2023|Halaman super admin adalah halaman untuk mengelola user |
| 5 |Melakukan Switching menjadi sistem Admin|24 Maret 2023|Setelah menimabang beerapa aspek saya berpindah peran menjadi sistem admin|

Dokumentasi : 

* Membuat fungsi login
![laman login](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/raw/main/ss/1_login.png)

* Membuat fungsi super admin untuk pengelolaan user
![Gambar teks editor VS Code](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/raw/main/ss/2._halaman_super_admin.png)

* Modal Super Admin
![Modal Super admin](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/raw/main/ss/3._Modal_Super_Admin.png)

* Halaman Tambah User
![halaman tambah User](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/raw/main/ss/4._membuat_halaman_tambah_user.png)

* Halaman Edit User
![Halaman Edit User](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/raw/main/ss/5._membuat_halaman_edit_user.png)

---

#### 2. Membuat layanan web server menggunakan Ubuntu, Nginx, mysql, dan php (24 Maret - 5 april 2023)

|No | Task | Tanggal | Keterangan | 
| - | :--: | :----------: | -------- |
| 1 |Instalasi dan konfigurasi nginx |24 - 27 Maret 2023 |Saat mengatur route agak sedikit kesulitan karena file untuk mengatur route error sehingga nginx tidak bisa running dengan baik|
| 2 |Instalasi dan konfigurasi mysql  |27 - 28 Maret 2023 |Terdapat masalah saat ingin mengkoneksikan aplikasi dengan database, database tidak dapat terhubung |
| 3 |Instalasi php v5.6 |27 - 28 Maret 2023 | |
| 4 |Mengatasi bug mysql / database tidak dapat diakses oleh aplikasi web pada server |28 - 30 Maret 2023 |Aplikasi sudah bisa berjalan dengan baik di server dan dapat diakses dengan ip tertentu |
| 5 |Upgrade Sistem Operasi Ubuntu Server |30 Maret - 5 April 2023 |Beberapa repository upgrade tidak bisa dipakai sehingga harus mencari repository lain yang bisa digunakan, hal ini yang menyebabkan proses upgrade menjadi lama |

Dokumentasi : 

* Instalasi Nginx
![Instalasi Nginx](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/raw/main/ss/2%20admin/1_nginx.png)

* Instalasi Mysql
![Instalasi Mysql](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/raw/main/ss/2%20admin/2_mysql.png)

* Instalasi Php v5.6
![Instalasi php5.6](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/raw/main/ss/2%20admin/3_php_-v.png)

* Upgrade Ubuntu ke 22.04
![ubuntu upgrade](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/raw/main/ss/2%20admin/4_versi_ubuntu.png)

* Hasil Web
![Hasil web](https://gitlab.com/apridan/apridan-husaeni-muharam-2000016001-magang2023/-/raw/main/ss/2%20admin/5_web.png)

